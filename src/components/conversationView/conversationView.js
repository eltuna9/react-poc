import React, {Component} from 'react';
import './styles.scss';

const propTypes = {};

class ConversationView extends Component {
  render() {
    return <div className="conversation-component">Hey there, I'm a conversation view!</div>;
  }
}

ConversationView.propTypes = propTypes;

export default ConversationView;
