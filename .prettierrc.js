module.exports = {
  printWidth: 120,
  parser: 'babylon',
  semi: true,
  singleQuote: true,
  useTabs: false,
  tabWidth: 2,
  bracketSpacing: false,
  jsxBracketSameLine: true
};
